import { StyleSheet, Text, View } from 'react-native';
import { Feather } from '@expo/vector-icons';
export default function Task() {
  return (
      <View style={styles.container}>
        <View style={styles.box}>
            <Text> 
                {/* <Feather name="plus" size={55} color="black" />  */}
                {/* <Feather name="minus" size={55} color="black" /> */}
                {/* <Feather name="divide" size={55} color="black" /> */}
                <Feather name="x" size={55} color="black" /> 
            </Text>
        </View>
        <View style={styles.box}>
            <Text style={styles.text}>
                117
            </Text>
        </View>
      </View>  
  );
}

const styles = StyleSheet.create({
    container:{
        paddingHorizontal: 15,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    box:{
        width: "45%",
        height: 100,
        borderColor: "#787ff3",
        borderWidth: 3,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 5,
        marginHorizontal: "2.3%"
    },
    text:{
        fontSize: 55,
    }
});

