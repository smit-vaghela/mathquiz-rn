import { StyleSheet, Text, View } from 'react-native';

export default function Timer() {
  return (
      <View style={styles.container}>
        <Text style={styles.timeFont}>Time: 3:00</Text>
        <View style={styles.progressbar}></View>
      </View>  
  );
}

const styles = StyleSheet.create({
    container:{
        paddingHorizontal: 15,
        justifyContent: "center",
        alignItems: "center",
    },
    progressbar:{
        height: 15,
        width:"100%" ,
        borderWidth: 3,
        borderColor: "#000",
        backgroundColor: "#5455ad",
    },
    timeFont:{
        fontSize: 20,
        fontWeight: "bold",
        alignSelf: "flex-end"
    }
});

