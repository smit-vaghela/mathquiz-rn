import { StyleSheet, Text, View } from 'react-native';

export default function Options() {
  return (
      <View style={styles.container}>
        <View style={styles.row}>
        <View style={styles.box}>
            <Text style={styles.num}>89</Text>
        </View>
        <View style={styles.box}>
            <Text style={styles.num}>90</Text>
        </View>
        </View>
        <View style={styles.row}>
        <View style={styles.box}>
            <Text style={styles.num}>89</Text>
        </View>
        <View style={styles.box}>
            <Text style={styles.num}>90</Text>
        </View>
        </View>
        <View style={styles.row}>
        <View style={styles.box}>
            <Text style={styles.num}>89</Text>
        </View>
        <View style={styles.box}>
            <Text style={styles.num}>90</Text>
        </View>
        </View>
      </View>  
  );
}

const styles = StyleSheet.create({
    container:{
        marginTop:10,
        marginHorizontal: 15,
        borderColor: "#000",
        borderWidth: 2,
    },
    row:{
        flexDirection: "row",
    },
    box:{
        width: "45%",
        height: 80,
        borderColor: "#787ff3",
        borderWidth: 3,
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 5,
        marginHorizontal: "2.3%"
    },

    num:{
        fontWeight: "700",
        fontSize: 40
    }
});

