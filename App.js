import { StatusBar } from 'expo-status-bar';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Timer from './components/Timer';
import Task from './components/Task';
import Options from './components/Options';

export default function App() {
  return (
    <View style={styles.container}>
    <SafeAreaView>
      
      <Timer/>
      <Task />
      <Options />
      <StatusBar style="auto" />
      
    </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff555',
    // alignItems: 'center',
    // justifyContent: 'center',
  },


});
